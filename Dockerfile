FROM python:slim
ARG CI_REPOSITORY_URL
RUN pip install --quiet --upgrade pip
RUN apt-get -q -y update
RUN apt-get -q -y upgrade
RUN apt-get -f -y install git
RUN git --version
RUN git config --global user.name "Docker in Docker"
RUN git config --global user.email "docker@dockerindocker.com"
RUN git clone $CI_REPOSITORY_URL